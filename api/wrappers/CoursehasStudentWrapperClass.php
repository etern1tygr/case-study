<?php
class CoursehasStudentWrapper{
  // Properties
  public $course;
  public $student;
  public $coursehastudent;


  function __construct() {
  }

  // Methods
  function set_student($student) {
    $this->student = $student;
  }
  function get_student() {
    return $this->$student;
  }
  function set_course($course) {
    $this->course = $course;
  }
  function get_course() {
    return $this->course;
  }
  function set_coursehastudent($coursehastudent) {
    $this->coursehastudent = $coursehastudent;
  }
  function get_coursehastudent() {
    return $this->coursehastudent;
  }
}
?>