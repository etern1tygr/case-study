<?php
class CoursehasTeacherWrapper{
  // Properties
  public $course;
  public $teacher;



  function __construct() {
  }

  // Methods
  function set_teacher($teacher) {
    $this->teacher = $teacher;
  }
  function get_teacher() {
    return $this->teacher;
  }
  function set_course($course) {
    $this->course = $course;
  }
  function get_course() {
    return $this->course;
  }
}
?>