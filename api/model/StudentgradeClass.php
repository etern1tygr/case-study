<?php
class Studentgrade {
  // Properties
  public $id;
  public $name;
  public $age;
  public $email;
  public $active;
  public $amount;


  function __construct() {
  }


  // Methods
  function set_id($id) {
    $this->id = $id;
  }
  function get_id() {
    return $this->id;
  }
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
  function set_age($age) {
    $this->age = $age;
  }
  function get_age() {
    return $this->age;
  }
  function set_email($email) {
    $this->email = $email;
  }
  function get_email() {
    return $this->email;
  }
  function set_active($active) {
    $this->active = $active;
  }
  function get_active() {
    return $this->active;
  }
  function set_amount($amount) {
    $this->amount = $amount;
  }
  function get_amount() {
    return $this->amount;
  }
}
?>