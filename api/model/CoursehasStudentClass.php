<?php
class CoursehasStudent {
  // Properties
  public $students_id;
  public $courses_id;
  public $amount;



  function __construct() {
  }

  // Methods
  function set_amount($amount) {
    $this->amount = $amount;
  }
  function get_amount() {
    return $this->amount;
  }
  function set_students_id($students_id) {
    $this->students_id = $students_id;
  }
  function get_students_id() {
    return $this->students_id;
  }
  function set_courses_id($courses_id) {
    $this->courses_id = $courses_id;
  }
  function get_courses_id() {
    return $this->courses_id;
  }
}
?>