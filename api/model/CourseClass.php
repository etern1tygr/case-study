<?php
class Course {
  // Properties
  public $id;
  public $title;
  public $weekday;
  public $start_time;
  public $end_time;
  public $active;

  function __construct() {
  }

  // Methods
  function set_id($id) {
    $this->id = $id;
  }
  function get_id() {
    return $this->id;
  }
  function set_title($title) {
    $this->title = $title;
  }
  function get_title() {
    return $this->title;
  }
  function set_weekday($weekday) {
    $this->weekday = $weekday;
  }
  function get_weekday() {
    return $this->weekday;
  }
  function set_start_time($start_time) {
    $this->start_time = $start_time;
  }
  function get_start_time() {
    return $this->start_time;
  }
  function set_end_time($end_time) {
    $this->end_time = $end_time;
  }
  function get_end_time() {
    return $this->end_time;
  }
  function set_active($active) {
    $this->active = $active;
  }
  function get_active() {
    return $this->active;
  }
}
?>