<?php
class Validator {
  // Properties

  function __construct() {
  }

  // Methods
  function is_set($target) {
    if(!isset($target)) return true;
    foreach($target as $key => $value ){
      if(!isset($key)) return true;
    }
   }
  function is_empty($target) {
    foreach($target as $key => $value ){
      if(empty($key)) return true;
    }
   }

  function is_get_entity($target) {
    if(!is_numeric($target['id'])){
      return true;
    }
  }
  function is_delete_course_teacher($target) {
    if(!is_numeric($target['courses_id'])){
      return true;
    }
    if(!is_numeric($target['teachers_id'])){
      return true;
    }  
  }
  function is_delete_course_student($target) {
    if(!is_numeric($target['courses_id'])){
      return true;
    }
    if(!is_numeric($target['students_id'])){
      return true;
    }  
  }
  function is_create_course_teacher($target) {
    if(!is_numeric($target['courses_id'])){
      return true;
    }
    if(!is_numeric($target['teachers_id'])){
      return true;
    }  
  }
  function is_create_course_student($target) {
    if(!is_numeric($target['courses_id'])){
      return true;
    }
    if(!is_numeric($target['students_id'])){
      return true;
    }  
  }
  function is_update_course_student($target) {
    if(!is_numeric($target['courses_id'])){
      return true;
    }
    if(!is_numeric($target['students_id'])){
      return true;
    }  
    if(!is_numeric($target['amount'])){
      return true;
    }  
  }

  function is_create_teacher($target) {
    if(!is_string($target['name'])){
      return true;
    }
    if(!filter_var($target['email'], FILTER_VALIDATE_EMAIL)){
      return true;
    }
    if(!is_numeric($target['age'])){
      return true;
    }  
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_get_Allteacher($target) {
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_update_teacher($target) {
    if(!is_numeric($target['id'])){
      return true;
    }
    if(!is_string($target['name'])){
      return true;
    }
    if(!filter_var($target['email'], FILTER_VALIDATE_EMAIL)){
      return true;
    }
    if(!is_numeric($target['age'])){
      return true;
    }  
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }

  function is_create_student($target) {
    if(!is_string($target['name'])){
      return true;
    }
    if(!filter_var($target['email'], FILTER_VALIDATE_EMAIL)){
      return true;
    }
    if(!is_numeric($target['age'])){
      return true;
    }  
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_get_Allstudent($target) {
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_update_student($target) {
    if(!is_numeric($target['id'])){
      return true;
    }
    if(!is_string($target['name'])){
      return true;
    }
    if(!filter_var($target['email'], FILTER_VALIDATE_EMAIL)){
      return true;
    }
    if(!is_numeric($target['age'])){
      return true;
    }  
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }

    
  function is_create_course($target) {
    if(!is_string($target['title'])){
      return true;
    }
    if(!is_string($target['start_time'])){
      return true;
    }
    if(!is_string($target['end_time'])){
      return true;
    }
    if(!is_string($target['weekday'])){
      return true;
    }
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_get_Allcourse($target) {
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
  function is_update_course($target) {
    if(!is_string($target['title'])){
      return true;
    }
    if(!is_string($target['start_time'])){
      return true;
    }
    if(!is_string($target['end_time'])){
      return true;
    }
    if(!is_string($target['weekday'])){
      return true;
    }
    if(!is_numeric($target['active'])){
      return true;
    }
    if(($target['active'] != 0) && $target['active'] != 1) return false;
  }
 
}
?>