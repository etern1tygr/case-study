<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_POST)){
    $data = "Values not set";

}
else if($val->is_empty($_POST)){
    $data = "Value empty";

}
else if($val->is_update_course($_POST)){
    $data = "Wrong active value";

}
else{
    $db = new Database();
    $db->connect();
    $course = new Course();
    $course->set_id($_POST['id']);
    $course->set_title($_POST['title']);
    $course->set_start_time($_POST['start_time']);
    $course->set_end_time($_POST['end_time']);
    $course->set_weekday($_POST['weekday']);
    $course->set_active($_POST['active']);
    $data = $db->updateCourse($course);

}

echo json_encode($data);


    
    
?>

