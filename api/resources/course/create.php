<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_POST)){
    $data = "Values not set";

}
else if($val->is_empty($_POST)){
    $data = "Value empty";

}
else if($val->is_create_course($_POST)){
    $data = "Not a course";

}
else{
    $db = new Database();
    $db->connect();
    $course = new Course();
    $course->set_title($_POST['title']);
    $format = 'Y-m-d H:i:s';
    $date = DateTime::createFromFormat($format, $_POST['start_time']);
    $course->set_start_time($date->format('Y-m-d H:i:s'));
    $date = DateTime::createFromFormat($format, $_POST['end_time']);
    $course->set_end_time($date->format('Y-m-d H:i:s'));
    $course->set_weekday($_POST['weekday']);
    $course->set_active($_POST['active']);
    $data = $db->insertCourse($course);

}


echo json_encode($data);


    
    
?>

