<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_POST)){
    $data = "Values not set";

}
else if($val->is_empty($_POST)){
    $data = "Value empty";

}
else if($val->is_create_student($_POST)){
    $data = "Not a student";

}
else{
    $db = new Database();
    $db->connect();
    $student = new Student();
    $student->set_name($_POST['name']);
    $student->set_age($_POST['age']);
    $student->set_email($_POST['email']);
    $student->set_active($_POST['active']);
    $data = $db->insertStudent($student);

}


echo json_encode($data);


    
    
?>

