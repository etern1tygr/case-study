<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../model/CoursehasStudentClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_POST)){
    $data = "Values not set";

}
else if($val->is_empty($_POST)){
    $data = "Value empty";

}
else if($val->is_create_course_student($_POST)){
    $data = "Values not Numeric";

}
else{
    $db = new Database();
    $db->connect();
    $coursestudent = new CoursehasStudent();
    $coursestudent->set_courses_id($_POST['courses_id']);
    $coursestudent->set_students_id($_POST['students_id']);
    $data = $db->insertCoursetoStudent($coursestudent);

}


echo json_encode($data);


    
    
?>

