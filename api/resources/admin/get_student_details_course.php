<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../model/CoursehasTeacherClass.php');
require_once('../../wrappers/CoursehasTeacherWrapperClass.php');
require_once('../../wrappers/CoursehasStudentWrapperClass.php');
require_once('../../model/StudentgradeClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_GET)){
    $data = "Values not set";

}
else if($val->is_empty($_GET)){
    $data = "Value empty";

}
else if($val->is_get_entity($_GET)){
    $data = "Value not Numeric";

}
else{
    $db = new Database();
    $db->connect();
    $wrapper = new CoursehasStudentWrapper();
    $course = new Course();
    $course->set_id($_GET['id']);
    $wrapper->set_course($course);
    $data = $db->getStudentDetailsCourse($wrapper);

}

    
echo json_encode($data);

?>

