<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../model/CoursehasTeacherClass.php');
require_once('../../wrappers/CoursehasStudentWrapperClass.php');
require_once('../../wrappers/CoursehasTeacherWrapperClass.php');
require_once('../../model/CoursehasStudentClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();


$val = new Validator();
if($val->is_set($_GET)){
    $data = "Values not set";

}
else if($val->is_empty($_GET)){
    $data = "Value empty";

}
else{
    $db = new Database();
    $db->connect();
    $wrapper = new CoursehasStudentWrapper();
    $wrapper1 = new CoursehasTeacherWrapper();
    $coursestudent = new CoursehasStudent();
    $teacher = new Teacher();
    $coursestudent->set_students_id($_GET['id']);
    $coursestudent->set_courses_id($_GET['courses_id']);
    $teacher->set_id($_GET['teachers_id']);
    $wrapper->set_coursehastudent($coursestudent);
    $wrapper1->set_teacher($teacher);
    $data = $db->getDetailsStudent($wrapper, $wrapper1);

}
    
echo json_encode($data);

?>

