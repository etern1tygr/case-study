<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../database/DatabaseClass.php');
require_once('../../model/TeacherClass.php');
require_once('../../model/StudentClass.php');
require_once('../../model/CourseClass.php');
require_once('../../helpers/ValidatorClass.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
session_start();

$val = new Validator();
if($val->is_set($_GET)){
    $data = "Values not set";

}
else if($val->is_empty($_GET)){
    $data = "Value empty";

}
else if($val->is_get_Allteacher($_GET)){
    $data = "Wrong active value";

}
else{
    $db = new Database();
    $db->connect();
    $teachers = array();
    $teacher = new Teacher();
    $teacher->set_active($_GET['active']);
    $data = $db->getallTeachers($teachers, $teacher);

}
echo json_encode($data);

?>

