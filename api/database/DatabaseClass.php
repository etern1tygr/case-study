<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Database {
  // Properties
  private const servername = "localhost";
  private const username = "root";
  private const password = "root";
  private const dbname = "lalizas";
  public $con;
  public $message;


  function __construct() {
  }
  
  function get_con() {
    return $this->con;
  }
  // Methods
  function connect () {
    try {
      $this->conn = new PDO("mysql:host=".self::servername.";dbname=".self::dbname."", self::username, self::password);
      // set the PDO error mode to exception
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->message = "Connected successfully";
    } catch(PDOException $e) {
      $this->message = "Connection failed: " . $e->getMessage();
    }  
  }

  function get_message() {
    return $this->message;
  }

  //Teacher implementation
  function getallTeachers($teachers, $teacher){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM teachers WHERE active=:active");
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $active = $teacher->get_active();
      if(!$stmt->execute()) return $teachers = array();    
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      foreach($results as $row) {
        $teachers[] = new Teacher();
        $teachers[$i]->set_id($row['id']);
        $teachers[$i]->set_name($row['name']);
        $teachers[$i]->set_age($row['age']);
        $teachers[$i]->set_email($row['email']);
        $teachers[$i]->set_active($row['active']);
        $i++;
      }
      return $teachers;

     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }
  function getTeacher($teacher){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM teachers WHERE id=:id");
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $id = $teacher->get_id();
      if(!$stmt->execute()) return $teacher = new Teacher();    
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row) {
        $teacher->set_name($row['name']);
        $teacher->set_age($row['age']);
        $teacher->set_email($row['email']);
        $teacher->set_active($row['active']);
      }
      return $teacher;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }
  function insertTeacher($teacher){
    try {
      $stmt = $this->conn->prepare("INSERT INTO teachers (name, age, email, active) VALUES (:name, :age, :email, :active)");
      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
      $stmt->bindParam(':age', $age, PDO::PARAM_INT);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);

      // insert one row
      $name = $teacher->get_name();
      $age = $teacher->get_age();
      $email = $teacher->get_email();
      $active = $teacher->get_active();
      if(!$stmt->execute()) return $teacher = new Teacher();    
      $id = $this->conn->lastInsertId();
      $teacher->set_id($id);
      return $teacher;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

    
  }
  function updateTeacher($teacher){
    try {
      $stmt = $this->conn->prepare("UPDATE teachers SET name=:name, age=:age, email=:email, active=:active WHERE id=:id");
      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
      $stmt->bindParam(':age', $age, PDO::PARAM_INT);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);

      // insert one row
      $name = $teacher->get_name();
      $age = $teacher->get_age();
      $email = $teacher->get_email();
      $active = $teacher->get_active();
      $id = $teacher->get_id();
      if(!$stmt->execute() || $stmt->rowCount() == 0) return $teacher = new Teacher();
      return $teacher;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }







  //Student implementation
  function getAllStudents($students, $student){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM students WHERE active=:active");
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $active = $student->get_active();
      if(!$stmt->execute()) return $students = array();     
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      foreach($results as $row) {
        $students[] = new Student();
        $students[$i]->set_id($row['id']);
        $students[$i]->set_name($row['name']);
        $students[$i]->set_age($row['age']);
        $students[$i]->set_email($row['email']);
        $students[$i]->set_active($row['active']);
        $i++;
      }
      return $students;
  
      } catch(PDOException $e) {
        return $this->message = $e->getMessage();
      }
    
  }
 
  function getStudent($student){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM students WHERE id=:id");
      $stmt->bindParam(':id', $id);
      $id = $student->get_id();
      if(!$stmt->execute()) return $student = new Student();     
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row) {
        $student->set_name($row['name']);
        $student->set_age($row['age']);
        $student->set_email($row['email']);
        $student->set_active($row['active']);
      }
      return $student;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }
  function insertStudent($student){
    try {
      $stmt = $this->conn->prepare("INSERT INTO students (name, age, email, active) VALUES (:name, :age, :email, :active)");
      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
      $stmt->bindParam(':age', $age, PDO::PARAM_INT);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);

      // insert one row
      $name = $student->get_name();
      $age = $student->get_age();
      $email = $student->get_email();
      $active = $student->get_active();
      if(!$stmt->execute()) return $student = new Student();     
      $id = $this->conn->lastInsertId();
      $student->set_id($id);
      return $student;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }

  function updateStudent($student){
    try {
      $stmt = $this->conn->prepare("UPDATE students SET name=:name, age=:age, email=:email, active=:active WHERE id=:id");
      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
      $stmt->bindParam(':age', $age, PDO::PARAM_INT);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);

      // insert one row
      $name = $student->get_name();
      $age = $student->get_age();
      $email = $student->get_email();
      $active = $student->get_active();
      $id = $student->get_id();
      if(!$stmt->execute() || $stmt->rowCount() == 0) return $student = new Student();
      return $student;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }


  }
  //Course implementation
  function getAllCourses($courses, $course){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM courses WHERE active=:active");
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $active = $course->get_active();
      if(!$stmt->execute()) $course = new Course();     
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      foreach($results as $row) {
        $courses[] = new Course();
        $courses[$i]->set_id($row['id']);
        $courses[$i]->set_title($row['title']);
        $courses[$i]->set_start_time($row['start_time']);
        $courses[$i]->set_end_time($row['end_time']);
        $courses[$i]->set_weekday($row['weekday']);
        $courses[$i]->set_active($row['active']);
        $i++;
      }
      return $courses;
  
      } catch(PDOException $e) {
        return $this->message = $e->getMessage();
      }
    
  }
 
  function getCourse($course){
    try {
      $stmt = $this->conn->prepare("SELECT* FROM courses WHERE id=:id");
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $id = $course->get_id();
      if(!$stmt->execute()) return $course = new Course();     
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row) {
        $course->set_title($row['title']);
        $course->set_start_time($row['start_time']);
        $course->set_end_time($row['end_time']);
        $course->set_weekday($row['weekday']);
        $course->set_active($row['active']);
      }
      return $course;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }
  function insertCourse($course){
    try {
      $stmt = $this->conn->prepare("INSERT INTO courses (title, start_time, end_time, weekday, active) VALUES (:title, :start_time, :end_time, :weekday, :active)");
      $stmt->bindParam(':title', $title, PDO::PARAM_STR);
      $stmt->bindParam(':start_time', $start_time, PDO::PARAM_STR);
      $stmt->bindParam(':end_time', $end_time, PDO::PARAM_STR);
      $stmt->bindParam(':weekday', $weekday, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);

      // insert one row
      $title = $course->get_title();
      $start_time = $course->get_start_time();
      $end_time = $course->get_end_time();
      $weekday = $course->get_weekday();
      $active = $course->get_active();
      if(!$stmt->execute()) return $course = new Course();     
      $id = $this->conn->lastInsertId();
      $course->set_id($id);
      return $course;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }
  function updateCourse($course){
    try {
      $stmt = $this->conn->prepare("UPDATE courses SET title=:title, start_time=:start_time, end_time=:end_time, weekday=:weekday, active=:active WHERE id=:id");
      $stmt->bindParam(':title', $title, PDO::PARAM_STR);
      $stmt->bindParam(':start_time', $start_time, PDO::PARAM_STR);
      $stmt->bindParam(':end_time', $end_time, PDO::PARAM_STR);
      $stmt->bindParam(':weekday', $weekday, PDO::PARAM_STR);
      $stmt->bindParam(':active', $active, PDO::PARAM_INT);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);

      // insert one row
      $title = $course->get_title();
      $start_time = $course->get_start_time();
      $end_time = $course->get_end_time();
      $weekday = $course->get_weekday();
      $active = $course->get_active();
      $id = $course->get_id();
      if(!$stmt->execute() || $stmt->rowCount() == 0) return $course = new Course();
      return $course;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }

  }
  //CourseHasTeacher implementation
  function insertCoursetoTeacher($courseteacher){
    try {
      $stmt = $this->conn->prepare("INSERT INTO courses_has_teachers (courses_id, teachers_id) VALUES (:courses_id, :teachers_id)");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $stmt->bindParam(':teachers_id', $teachers_id, PDO::PARAM_INT);

      // insert one row
      $courses_id = $courseteacher->get_courses_id();
      $teachers_id = $courseteacher->get_teachers_id();
      if(!$stmt->execute()) return $courseteacher = new CoursehasTeacher();     
      return $courseteacher;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }
  function deleteCoursetoTeacher($courseteacher){
    try {
      $stmt = $this->conn->prepare("DELETE FROM courses_has_teachers WHERE teachers_id=:teachers_id AND courses_id=:courses_id");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $stmt->bindParam(':teachers_id', $teachers_id, PDO::PARAM_INT);

      // insert one row
      $courses_id = $courseteacher->get_courses_id();
      $teachers_id = $courseteacher->get_teachers_id();
      if(!$stmt->execute())  return $coursesteacher = new CoursehasTeacher();     
      return $courseteacher;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }


  //CourseHasStudent implementation
  function insertCoursetoStudent($coursestudent){
    try {
      $stmt = $this->conn->prepare("INSERT INTO courses_has_students (courses_id, students_id) VALUES (:courses_id, :students_id)");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $stmt->bindParam(':students_id', $students_id, PDO::PARAM_INT);

      // insert one row
      $courses_id = $coursestudent->get_courses_id();
      $students_id = $coursestudent->get_students_id();
      if(!$stmt->execute()) return $coursestudent = new CoursehasStudent();     
      return $coursestudent;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
  }
  function updateCoursetoStudent($coursestudent){
    try {
      $stmt = $this->conn->prepare("UPDATE courses_has_students SET amount=:amount WHERE students_id=:students_id AND courses_id=:courses_id");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $stmt->bindParam(':students_id', $students_id, PDO::PARAM_INT);
      $stmt->bindParam(':amount', $amount, PDO::PARAM_INT);

      // insert one row
      $courses_id = $coursestudent->get_courses_id();
      $students_id = $coursestudent->get_students_id();
      $amount = $coursestudent->get_amount();
      if(!$stmt->execute() || $stmt->rowCount() == 0)  return $coursestudent = new CoursehasStudent();     
      return $coursestudent;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }
  function deleteCoursetoStudent($coursestudent){
    try {
      $stmt = $this->conn->prepare("DELETE FROM courses_has_students WHERE students_id=:students_id AND courses_id=:courses_id");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $stmt->bindParam(':students_id', $students_id, PDO::PARAM_INT);

      // insert one row
      $courses_id = $coursestudent->get_courses_id();
      $students_id = $coursestudent->get_students_id();
      if(!$stmt->execute())  return $coursestudent = new CoursehasStudent();     
      return $courseteacher;

    }
    catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
    
  }


  //CourseDetails implementation
  function getStudentDetailsCourse($wrapper){
    try {
      $stmt = $this->conn->prepare("SELECT *, students.id AS tid, students.active AS tactive, students.id AS cid, students.active AS cactive FROM courses INNER JOIN courses_has_students ON courses.id = courses_has_students.courses_id INNER JOIN students ON students.id = courses_has_students.students_id WHERE courses.id=:courses_id");
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $courses_id = $wrapper->get_course()->get_id();
      if(!$stmt->execute()) return $wrapper;    
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      $students = array();
      $course = new Course();
      foreach($results as $row) {
        $student = new Studentgrade();
        $course->set_id($row['tid']);
        $course->set_title($row['title']);
        $course->set_start_time($row['start_time']);
        $course->set_end_time($row['end_time']);
        $course->set_weekday($row['weekday']);
        $course->set_active($row['tactive']);
        if(isset($row['cid'])){
          $student->set_id($row['tid']);
          $student->set_name($row['name']);
          $student->set_age($row['age']);
          $student->set_email($row['email']);
          $student->set_active($row['tactive']);
          $student->set_amount($row['amount']);
          $students[$i] = $student;
        }
        $i++;
      }
      $wrapper->set_course($course);
      $wrapper->set_student($students);

      return $wrapper;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
  }
  function getCourseDetailsTeacher($wrapper){
    try {
      $stmt = $this->conn->prepare("SELECT *, teachers.id AS tid, teachers.active AS tactive, courses.id AS cid, courses.active AS cactive FROM teachers INNER JOIN courses_has_teachers ON teachers.id = teachers_id INNER JOIN courses ON courses.id = courses_id WHERE teachers.id=:teachers_id");
      $stmt->bindParam(':teachers_id', $teachers_id, PDO::PARAM_INT);
      $teachers_id = $wrapper->get_teacher()->get_id();
      if(!$stmt->execute()) return $wrapper;    
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      $courses = array();
      $teacher = new Teacher();
      foreach($results as $row) {
        $course = new Course();
        $teacher->set_id($row['tid']);
        $teacher->set_name($row['name']);
        $teacher->set_age($row['age']);
        $teacher->set_email($row['email']);
        $teacher->set_active($row['tactive']);
        if(isset($row['cid'])){
          $course->set_id($row['cid']);
          $course->set_title($row['title']);
          $course->set_start_time($row['start_time']);
          $course->set_end_time($row['end_time']);
          $course->set_weekday($row['weekday']);
          $course->set_active($row['cactive']);
          $courses[$i] = $course;
        }
        $i++;
      }
      $wrapper->set_teacher($teacher);
      $wrapper->set_course($courses);

      return $wrapper;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
  }


  //StudentDetails implementation
  function getDetailsStudent($wrapper, $wrapper1){
    try {
      $stmt = $this->conn->prepare("SELECT *, students.id AS sid, students.active AS sactive, courses.id AS cid, courses.active AS cactive FROM students LEFT JOIN courses_has_students ON students.id = students_id LEFT JOIN courses ON courses.id = courses_id LEFT JOIN courses_has_teachers ON courses_has_teachers.courses_id = courses_has_students.courses_id WHERE students.id=:students_id AND courses_has_students.courses_id=:courses_id AND courses_has_teachers.teachers_id=:teachers_id");
      $stmt->bindParam(':students_id', $students_id, PDO::PARAM_INT);
      $stmt->bindParam(':teachers_id', $teachers_id, PDO::PARAM_INT);
      $stmt->bindParam(':courses_id', $courses_id, PDO::PARAM_INT);
      $students_id = $wrapper->get_coursehastudent()->get_students_id();
      $courses_id = $wrapper->get_coursehastudent()->get_courses_id();
      $teachers_id = $wrapper1->get_teacher()->get_id();
      if(!$stmt->execute()) return $wrapper;    
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $i = 0;
      $courses = array();
      $student = new Student();
      $grades = array();
      foreach($results as $row) {
        $course = new Course();
        $grade = new CoursehasStudent();
        $student->set_id($row['sid']);
        $student->set_name($row['name']);
        $student->set_age($row['age']);
        $student->set_email($row['email']);
        $student->set_active($row['sactive']);
        if(isset($row['cid'])){
          $course->set_id($row['cid']);
          $course->set_title($row['title']);
          $course->set_start_time($row['start_time']);
          $course->set_end_time($row['end_time']);
          $course->set_weekday($row['weekday']);
          $course->set_active($row['cactive']);
          $grade->set_amount($row['amount']);
          $grade->set_courses_id($row['cid']);
          $grade->set_students_id($row['sid']);
          $grades[$i] = $grade;
          $courses[$i] = $course;
        }
        $i++;
      }
      $wrapper->set_student($student);
      $wrapper->set_course($courses);
      $wrapper->set_coursehastudent($grades);

      return $wrapper;     

    } catch(PDOException $e) {
      return $this->message = $e->getMessage();
    }
  }


}
?>